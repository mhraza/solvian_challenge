<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190503135343 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE price_history (id INT AUTO_INCREMENT NOT NULL, certificate_id_id INT NOT NULL, issuing_price DOUBLE PRECISION NOT NULL, current_price DOUBLE PRECISION NOT NULL, price_date_time DATETIME NOT NULL, INDEX IDX_4C9CB81756E34EF2 (certificate_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE certificate_documents (id INT AUTO_INCREMENT NOT NULL, certificate_id_id INT NOT NULL, name VARCHAR(1000) NOT NULL, path VARCHAR(1000) NOT NULL, created_at DATETIME NOT NULL, INDEX IDX_F6781CCD56E34EF2 (certificate_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE price_history ADD CONSTRAINT FK_4C9CB81756E34EF2 FOREIGN KEY (certificate_id_id) REFERENCES certificates (id)');
        $this->addSql('ALTER TABLE certificate_documents ADD CONSTRAINT FK_F6781CCD56E34EF2 FOREIGN KEY (certificate_id_id) REFERENCES certificates (id)');
        $this->addSql('ALTER TABLE certificates ADD barrier_level DOUBLE PRECISION DEFAULT NULL, ADD participation_rate DOUBLE PRECISION NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE price_history');
        $this->addSql('DROP TABLE certificate_documents');
        $this->addSql('ALTER TABLE certificates DROP barrier_level, DROP participation_rate');
    }
}
