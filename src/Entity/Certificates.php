<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CertificatesRepository")
 */
class Certificates
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $isin;

    /**
     * @ORM\Column(type="string", length=1000)
     */
    private $trading_market;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $currency;

    /**
     * @ORM\Column(type="string", length=1000)
     */
    private $issuer;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $certificate_type;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $barrier_level;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $participation_rate;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PriceHistory", mappedBy="certificate_id", orphanRemoval=true)
     */
    private $priceHistories;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CertificateDocuments", mappedBy="certificate_id", orphanRemoval=true)
     */
    private $certificate_documents;

    public function __construct()
    {
        $this->priceHistories = new ArrayCollection();
        $this->certificate_documents = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIsin(): ?int
    {
        return $this->isin;
    }

    public function setIsin(int $isin): self
    {
        $this->isin = $isin;

        return $this;
    }

    public function getTradingMarket(): ?string
    {
        return $this->trading_market;
    }

    public function setTradingMarket(string $trading_market): self
    {
        $this->trading_market = $trading_market;

        return $this;
    }

    public function getCurrency(): ?string
    {
        return $this->currency;
    }

    public function setCurrency(string $currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    public function getIssuer(): ?string
    {
        return $this->issuer;
    }

    public function setIssuer(string $issuer): self
    {
        $this->issuer = $issuer;

        return $this;
    }

    public function getCertificateType(): ?string
    {
        return $this->certificate_type;
    }

    public function setCertificateType(?string $certificate_type): self
    {
        $this->certificate_type = $certificate_type;

        return $this;
    }

    public function getBarrierLevel(): ?float
    {
        return $this->barrier_level;
    }

    public function setBarrierLevel(?float $barrier_level): self
    {
        $this->barrier_level = $barrier_level;

        return $this;
    }

    public function getParticipationRate(): ?float
    {
        return $this->participation_rate;
    }

    public function setParticipationRate(float $participation_rate): self
    {
        $this->participation_rate = $participation_rate;

        return $this;
    }

    /**
     * @return Collection|PriceHistory[]
     */
    public function getPriceHistories(): Collection
    {
        return $this->priceHistories;
    }

    public function addPriceHistory(PriceHistory $priceHistory): self
    {
        if (!$this->priceHistories->contains($priceHistory)) {
            $this->priceHistories[] = $priceHistory;
            $priceHistory->setCertificateId($this);
        }

        return $this;
    }

    public function removePriceHistory(PriceHistory $priceHistory): self
    {
        if ($this->priceHistories->contains($priceHistory)) {
            $this->priceHistories->removeElement($priceHistory);
            // set the owning side to null (unless already changed)
            if ($priceHistory->getCertificateId() === $this) {
                $priceHistory->setCertificateId(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|CertificateDocuments[]
     */
    public function getCertificateDocuments(): Collection
    {
        return $this->certificate_documents;
    }

    public function addCertificateDocument(CertificateDocuments $certificateDocument): self
    {
        if (!$this->certificate_documents->contains($certificateDocument)) {
            $this->certificate_documents[] = $certificateDocument;
            $certificateDocument->setCertificateId($this);
        }

        return $this;
    }

    public function removeCertificateDocument(CertificateDocuments $certificateDocument): self
    {
        if ($this->certificate_documents->contains($certificateDocument)) {
            $this->certificate_documents->removeElement($certificateDocument);
            // set the owning side to null (unless already changed)
            if ($certificateDocument->getCertificateId() === $this) {
                $certificateDocument->setCertificateId(null);
            }
        }

        return $this;
    }
}
