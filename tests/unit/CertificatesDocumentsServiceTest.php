<?php

use \App\BusinessLogic\CertificatesDocuments\CertificatesDocumentsService;
use \App\BusinessLogic\Certificates\CertificatesService;

class CertificatesDocumentsServiceTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * @var CertificatesDocumentsService
     */
    protected $certificatesDocumentsService;

    /**
     * @var CertificatesService
     */
    protected $certificateService;

    /**
     * @throws \Codeception\Exception\ModuleException
     */
    protected function _before()
    {
        $this->certificatesDocumentsService = $this->getModule('Symfony')->_getContainer()->get('App\BusinessLogic\CertificatesDocuments\CertificatesDocumentsService');
        $this->certificateService = $this->getModule('Symfony')->_getContainer()->get('App\BusinessLogic\Certificates\CertificatesService');

    }

    /**
     * Add certificate documents
     */
    public function testAddCertificateDocument()
    {
        $fields = array(
            'isin' => '1234',
            'trading_market' => 'test market',
            'issuer' => 'test issuer',
            'currency' => 'eur',
            'current_price' => 10.0,
            'issuing_price' => 5.10,
        );
        $certificate = $this->certificateService->addCertificates($fields);
        $params = array(
            'name' => 'test document',
            'path' => 'test path',
            'document_type' => 'test type',
            'certificate' => $certificate['data']
        );
        $response = $this->certificatesDocumentsService->addDocument($params);
        $this->tester->assertEquals(201, $response['status']);
    }
}