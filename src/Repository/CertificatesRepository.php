<?php

namespace App\Repository;

use App\Entity\Certificates;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Certificates|null find($id, $lockMode = null, $lockVersion = null)
 * @method Certificates|null findOneBy(array $criteria, array $orderBy = null)
 * @method Certificates[]    findAll()
 * @method Certificates[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CertificatesRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Certificates::class);
    }

    /**
     * @param $params
     * @return array
     */
    public function addCertificates($params)
    {
        try{
            $certificates = new Certificates();
            $certificates->setIsin($params['isin']);
            $certificates->setTradingMarket($params['trading_market']);
            $certificates->setIssuer($params['issuer']);
            $certificates->setCurrency($params['currency']);
            if (isset($params['participation_rate'])) {
                $certificates->setParticipationRate($params['participation_rate']);
                $certificates->setCertificateType("guarantee-certificate");
            }
            elseif (isset($params['barrier_level'])) {
                $certificates->setBarrierLevel($params['barrier_level']);
                $certificates->setCertificateType("bonus-certificate");
            } else {
                $certificates->setCertificateType("standard-certificate");
            }
            $this->_em->persist($certificates);
            $this->_em->flush($certificates);

            return array(
                'status' => 201,
                'data' => $certificates
            );
        } catch (\Doctrine\ORM\ORMException | \Exception | \Doctrine\ORM\OptimisticLockException $ex){

            return array(
                'status' => 500,
                'data' => $ex->getMessage()
            );
        }
    }

    /**
     * @param Certificates $certificate
     * @return array
     */
    public function removeCertificate(Certificates $certificate)
    {
        try{
            $this->_em->remove($certificate);
            $this->_em->flush($certificate);

            return array(
                'status' => 202,
                'data' => "Removed sucessfully"
            );

        } catch (\Doctrine\ORM\ORMException | \Exception | \Doctrine\ORM\OptimisticLockException $ex){

            return array(
                'status' => 500,
                'data' => $ex->getMessage()
            );
        }
    }
}
