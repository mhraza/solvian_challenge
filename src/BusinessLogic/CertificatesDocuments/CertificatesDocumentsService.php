<?php
/**
 * Created by PhpStorm.
 * User: naeem
 * Date: 04.05.19
 * Time: 00:42
 */

namespace App\BusinessLogic\CertificatesDocuments;


use App\Entity\CertificateDocuments;
use App\Repository\CertificateDocumentsRepository;

class CertificatesDocumentsService
{
    /**
     * @var CertificateDocumentsRepository
     */
    protected $certificatesDocumentsRepository;

    /**
     * CertificatesDocumentsService constructor.
     * @param CertificateDocumentsRepository $certificatesDocumentsRepository
     */
    public function __construct(CertificateDocumentsRepository $certificatesDocumentsRepository)
    {
        $this->certificatesDocumentsRepository = $certificatesDocumentsRepository;
    }

    /**
     * @param array $params
     * @return array|null
     */
    public function addDocument(array $params) :?array
    {
        if (empty($params['document_type'])){

            return array(
                'data' => 'document type required',
                'status' => 403
            );
        }
        return $this->certificatesDocumentsRepository->addDocument($params);
    }

}